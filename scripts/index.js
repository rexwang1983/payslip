angular.module('App', []).controller('indexCtrl', function($scope, $http) {
	// Initial months
	$scope.months = [{name: "January"},
					 {name: "February"},
					 {name: "March"},
					 {name: "April"},
					 {name: "May"},
					 {name: "June"},
					 {name: "July"},
					 {name: "August"},
					 {name: "September"},
					 {name: "October"},
					 {name: "November"},
					 {name: "December"}]
	// Initial persons
	$scope.persons = [{firstName: "", 
	                   lastName: "",
	                   annualSalary: 18000,
	                   superRate: 0,
	                   paymentStartDate: "January"}];
	// Initial payslips
	$scope.payslips = [];
	// Initial tax table
    $scope.taxTable = [{moneyLine: 18200, moneyPlus:0, rate: 0.19},
                       {moneyLine: 37000, moneyPlus:3572, rate: 0.325},
                       {moneyLine: 80000, moneyPlus:17547, rate: 0.37},
                       {moneyLine: 180000, moneyPlus:54547, rate: 0.45}];

    // Add person infomation
	$scope.addPerson = function() {
    	$scope.persons.push({
                       firstName: "", 
	                   lastName: "",
	                   annualSalary: 18000,
	                   superRate: 0,
	                   paymentStartDate: "January"});
    }
    
    // Generate payslip for all input persons
    $scope.generatePayslip = function() {
    	$scope.payslips = [];
    	for (var i=0; i<$scope.persons.length; i++){
    		$scope.payslips.push($scope.getPayslip($scope.persons[i]));
    	}
    }

    // Get payslip infomation
    $scope.getPayslip = function(person) {
    	var name = person.firstName + ' ' + person.lastName;
    	var payPeriod = person.paymentStartDate;
    	var grossIncome = Math.round(person.annualSalary / 12);
    	var incomeTax = $scope.calculateTax(person.annualSalary);
    	var netIncome = grossIncome - incomeTax;
    	var superVaule = Math.round(grossIncome * person.superRate / 100);
    	return {
    		name: name,
    		payPeriod: payPeriod,
    		grossIncome: grossIncome,
    		incomeTax: incomeTax,
    		netIncome: netIncome,
    		super: superVaule};
    }

    // Calculate tax
    $scope.calculateTax = function(annualSalary){
    	var incomeTax = 0;
    	for (var i = $scope.taxTable.length - 1; i >= 0; i--) {
    		if (annualSalary>$scope.taxTable[i].moneyLine) {
            	incomeTax = Math.round(($scope.taxTable[i].moneyPlus
            		                  +(annualSalary - $scope.taxTable[i].moneyLine)
            		                  *$scope.taxTable[i].rate)/12);
            	break;
    		}
    	}
    	return incomeTax;
    }
})
