
describe('test indexCtrl', function() {

  describe('indexCtrl', function() {
    var $scope;

    beforeEach(module('App'));

    beforeEach(inject(function($rootScope, $controller) {
      $scope = $rootScope.$new();
      $controller('indexCtrl', {$scope: $scope});
    }));

    it('Comparing results', function() {
      // Initial test data
      var persons = [{firstName: "David", 
                    lastName: "Rudd",
                    annualSalary: 60050,
                    superRate: 9,
                    paymentStartDate: "01 March-31 March"},
                    {firstName: "Ryan", 
                    lastName: "Chen",
                    annualSalary: 120000,
                    superRate: 10,
                    paymentStartDate: "01 March-31 March"}];
      // Initial test result
      var results = [{name: "David Rudd",
                    payPeriod: "01 March-31 March",
                    grossIncome: 5004,
                    incomeTax: 922,
                    netIncome: 4082,
                    super: 450},
                    {name: "Ryan Chen",
                    payPeriod: "01 March-31 March",
                    grossIncome: 10000,
                    incomeTax: 2696,
                    netIncome: 7304,
                    super: 1000}];
      // Compare result
      for(var i=0;i<persons.length;i++){
          expect($scope.getPayslip(persons[1])).toEqual(results[1]);
      }
    });
  });
});